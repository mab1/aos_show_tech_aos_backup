# Generate aos_show_tech and aos_backup:

- Generate `aos_show_tech` and `aos_backup` automatically and create a local copy.

```
mab@Mehdis-MacBook-Pro-2 ~/mab_lab/aos_show_tech_aos_backup $ docker run --rm -it -v $(pwd):/ansible/playbooks ansible-2.8:latest pb_aos_diag.yaml 
 [WARNING]: Invalid characters were found in group names but not replaced, use -vvvv to see details


PLAY [Get diag from AOS server] ***********************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************
ok: [172.20.64.3]

TASK [Get AOS version] ********************************************************************************************************************************

TASK [get-aos-version : Execute command] **************************************************************************************************************
changed: [172.20.64.3]

TASK [get-aos-version : Register result] **************************************************************************************************************
ok: [172.20.64.3]

TASK [get-aos-version : Display version] **************************************************************************************************************
ok: [172.20.64.3] => {
    "msg": [
        "VERSION=99.2.0-master-5857", 
        "BUILD_ID=AOS_latest_OB.5857", 
        "BRANCH_NAME=master", 
        "COMMIT_ID=b874bba1c3d6d3e37638ea916d4aa8e60f542fda", 
        "BUILD_DATETIME=2019-07-01_20:28:37_PDT", 
        "AOS_DI_RELEASE=3-33"
    ]
}

TASK [get-aos-version : Store AOS version] ************************************************************************************************************
ok: [172.20.64.3]

TASK [Get devices list] *******************************************************************************************************************************

TASK [get-device-list : Login to AOS server (API)] ****************************************************************************************************
ok: [172.20.64.3]

TASK [get-device-list : Get Systems] ******************************************************************************************************************
ok: [172.20.64.3]

TASK [get-device-list : Logout from AOS server (API)] *************************************************************************************************
ok: [172.20.64.3]

TASK [get-device-list : Create a device inventoy] *****************************************************************************************************
ok: [172.20.64.3]

TASK [get-device-list : Display device inventory] *****************************************************************************************************
ok: [172.20.64.3] => {
    "msg": [
        {
            "device_key": "5254006B1CDC", 
            "hw_model": "VX", 
            "mgmt_ipaddr": "172.20.64.11", 
            "serial_number": "5254006B1CDC", 
            "vendor": "Cumulus"
        }, 
        {
            "device_key": "525400F24166", 
            "hw_model": "Generic Model", 
            "mgmt_ipaddr": "172.20.64.7", 
            "serial_number": "525400F24166", 
            "vendor": "Generic Manufacturer"
        }, 
        {
            "device_key": "52540096739E", 
            "hw_model": "VX", 
            "mgmt_ipaddr": "172.20.64.12", 
            "serial_number": "52540096739E", 
            "vendor": "Cumulus"
        }, 
        {
            "device_key": "5254004629DA", 
            "hw_model": "VX", 
            "mgmt_ipaddr": "172.20.64.10", 
            "serial_number": "5254004629DA", 
            "vendor": "Cumulus"
        }, 
        {
            "device_key": "5254008EAE80", 
            "hw_model": "VX", 
            "mgmt_ipaddr": "172.20.64.13", 
            "serial_number": "5254008EAE80", 
            "vendor": "Cumulus"
        }, 
        {
            "device_key": "5254001DA7A6", 
            "hw_model": "Generic Model", 
            "mgmt_ipaddr": "172.20.64.6", 
            "serial_number": "5254001DA7A6", 
            "vendor": "Generic Manufacturer"
        }, 
        {
            "device_key": "52540025D42E", 
            "hw_model": "VX", 
            "mgmt_ipaddr": "172.20.64.9", 
            "serial_number": "52540025D42E", 
            "vendor": "Cumulus"
        }
    ]
}

TASK [Get aos_show_tech] ******************************************************************************************************************************

TASK [get-aos-show-tech : Execute command (takes some time ...)] **************************************************************************************
changed: [172.20.64.3]

TASK [get-aos-show-tech : Identify file name] *********************************************************************************************************
ok: [172.20.64.3]

TASK [get-aos-show-tech : Display file name] **********************************************************************************************************
ok: [172.20.64.3] => {
    "msg": "\"aos_show_tech_20190707_170926.tar.gz\"\n"
}

TASK [get-aos-show-tech : Copy file locally] **********************************************************************************************************
changed: [172.20.64.3]

TASK [Get aos_backup] *********************************************************************************************************************************

TASK [get-aos-backup : Execute command (takes some time ...)] *****************************************************************************************
changed: [172.20.64.3]

TASK [get-aos-backup : Identify file name] ************************************************************************************************************
ok: [172.20.64.3]

TASK [get-aos-backup : Display folder name] ***********************************************************************************************************
ok: [172.20.64.3] => {
    "msg": "\"2019-07-07_17-12-48\"\n"
}

TASK [get-aos-backup : Copy files locally] ************************************************************************************************************
changed: [172.20.64.3] => (item=aos.data.tar.gz)
changed: [172.20.64.3] => (item=aos_restore)
changed: [172.20.64.3] => (item=comment.txt)

PLAY RECAP ********************************************************************************************************************************************
172.20.64.3                : ok=18   changed=5    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

mab@Mehdis-MacBook-Pro-2 ~/mab_lab/aos_show_tech_aos_backup $
```

## Result:
```
mab@Mehdis-MacBook-Pro-2 ~/mab_lab/aos_show_tech_aos_backup $ tree 172.20.64.3/
172.20.64.3/
├── home
│   └── admin
│       ├── aos_show_tech_20190707_170926.tar.gz
│       └── aos_show_tech_20190707_171458.tar.gz
└── var
    └── lib
        └── aos
            └── snapshot
                ├── 2019-07-07_17-12-48
                │   ├── aos.data.tar.gz
                │   ├── aos_restore
                │   └── comment.txt
                └── 2019-07-07_17-18-03
                    ├── aos.data.tar.gz
                    ├── aos_restore
                    └── comment.txt

8 directories, 8 files
```
